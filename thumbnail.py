from PIL import Image
from PIL import _imaging
import os  
import logging
import boto3
from botocore.exceptions import ClientError
import sys

max_width = 256
max_height = 256
image_format = "jpg"
output_dir = "/tmp/thumbnails"
output_bucket = "sean-halupczynski-thumbnail-images"
input_bucket = "sean-halupczynski-original-images"
original_image = sys.argv[1]

def handler(event, context):  
  bucket = event['Records'][0]['s3']['bucket']['name']
  key = event['Records'][0]['s3']['object']['key']

  try:
    #tmp = "/tmp/" + key
    tmp = key
    download(bucket, key, tmp)

    # resize
    resized = resized_path(key)
    resize(tmp, resized)

    # upload resized image
    upload(resized, output_bucket, get_filename(key) + ".jpg")

    return  

  except Exception as e:  
      print(e)  
      
def resized_path(key):
  name = get_filename(os.path.basename(key))
  return '/tmp/{}.{}'.format(name, image_format)

def resize(src, dest):
  # pil image processing
  img = Image.open(src, 'r')  
  img.thumbnail((max_width, max_height), Image.ANTIALIAS)  
  print('format: ' + img.format)
  if img.format == 'PNG' and image_format == 'jpg':
    img = img.convert('RGB')
  print('image resized')
  return img.save(dest)

def download(source_bucket, source_object_key, tmp):
  """ Download the specified object to the tmp location """
  s3 = boto3.client('s3')
  s3.download_file(source_bucket, source_object_key, tmp)

def upload(src, dest_bucket, dest_object):
  """ Upload the specified src file to the specific object"""
  if dest_object is None:
    dest_object = src

  s3_client = boto3.client('s3')
  try:
    response = s3_client.upload_file(src, dest_bucket, dest_object)
  except ClientError:
    logging.error(ClientError)
    return False
  return True

def get_filename(key):
  filename = os.path.basename(key)  
  name, ext = os.path.splitext(filename)
  return name


if __name__ == "__main__":
  event = {
    "bucket": input_bucket,
    # "object": "flower.png",
    "object": original_image,
  }
  handler(event, None)
